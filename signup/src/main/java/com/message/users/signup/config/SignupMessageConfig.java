package com.message.users.signup.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.message.users.signup.service.impl.SignupMessageListener;

@Configuration
public class SignupMessageConfig {

	@Value("${rabbitmq.exchangeName}")
	private String exchangeName;
	@Value("${rabbitmq.routingKey}")
	private String routingKey;
	@Value("${rabbitmq.queueName}")
	private String queueName;
	

	/**
	 * This method will create a RabbitMQ queue based on the configuration
	 * provided.
	 * @return
	 */
	@Bean
	Queue queue() {
		return new Queue(queueName);
	}

	/**
	 * This method will configure the Exchange service as TopicExchange.
	 * @return
	 */
	@Bean
	TopicExchange exchange() {
		return new TopicExchange(exchangeName);
	}
	
	/**
	 * This method will configure the Queue with Exchange using routingKey.
	 * @param queue
	 * @param exchange
	 * @return
	 */
	@Bean
	Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(routingKey);
	}
	
	/**
	 * This method will be used for the Transformation of JSON object to Bean Object.
	 * @return
	 */
	@Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
        return new MappingJackson2MessageConverter();
    }

	/**
	 * This method will create a RabbitMQTemplate by using ConnectionFactory
	 * @param connectionFactory
	 * @return
	 */
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        rabbitTemplate.setRoutingKey(routingKey);
        rabbitTemplate.setExchange(exchangeName);
        return rabbitTemplate;
    }
    
    /**
     * Setting up our SignupMessageListener to MessageListenerAdapter
     * @param SignupMessageListener
     * @return
     */
    @Bean
	MessageListenerAdapter listenerAdapter(SignupMessageListener messageListener) {
		return new MessageListenerAdapter(messageListener);
	}
    
	
    /**
	 * The Listener will adapt the MessageListener and will be registered on startup and invoked
	 * when the request comes.
	 * @param connectionFactory
	 * @param listenerAdapter
	 * @return
	 */
	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
			MessageListenerAdapter messageListenerAdapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames(queueName);
		container.setMessageListener(messageListenerAdapter);
		return container;
	}
	 
	 
	/**
	 * This method will create thread pool using ThreadPoolTaskExecutor.
	 * The max size of the thread pool is 10.
	 * @return TaskExecutor
	 */
	@Bean(name = "taskExecutor")
    public TaskExecutor getAsyncTaskExecuter() {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("SignupPool-");
        executor.initialize();
        return executor;
    }

	
}