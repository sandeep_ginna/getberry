package com.message.users.signup.service;

import com.message.users.signup.model.SignupUserDetailsBean;

public interface SignupMessagePublisher {

	void publish(SignupUserDetailsBean userDetailsBean);
}
