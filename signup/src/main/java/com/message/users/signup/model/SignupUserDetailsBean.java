package com.message.users.signup.model;

public class SignupUserDetailsBean {
	
	private String fullName;
    private String firstName;
    private String lastName;
    private String phone;
    
    public SignupUserDetailsBean() {};

    public SignupUserDetailsBean(Builder builder) {
    	setFullName(builder.fullName);
    	setFirstName(builder.firstName);
    	setLastName(builder.lastName);
    	setPhone(builder.phone);
    }
    
    public static Builder newSignupUserDetailsBean() {
        return new Builder();
    }

	public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public static final class Builder {
    	
    	private String fullName;
        private String firstName;
        private String lastName;
        private String phone;
        
        private Builder() {};
        
        public Builder fullName(String fullName) {
        	this.fullName = fullName;
        	return this;
        }
        
        public Builder firstName(String firstName) {
        	this.firstName = firstName;
        	return this;
        }
        
        public Builder lastName(String lastName) {
        	this.lastName = lastName;
        	return this;
        }
        
        public Builder phone(String phone) {
        	this.phone = phone;
        	return this;
        }
        
        public SignupUserDetailsBean build() {
            return new SignupUserDetailsBean(this);
        }
    	
    }
}
