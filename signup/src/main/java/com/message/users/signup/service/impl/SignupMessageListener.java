package com.message.users.signup.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.message.users.signup.model.SignupUserDetailsBean;
import com.message.users.signup.util.SignupConstants;
import com.message.users.signup.util.SignupUtil;

@Service
public class SignupMessageListener {

    private static final Logger LOG = LogManager.getLogger(SignupMessageListener.class);
   
    @Autowired
	private TaskExecutor taskExecutor;
    
    /**It will pick the message from Queue, this method  will be invoked when the queue has a message 
     * 
     * @param byte[]
     */
    public void handleMessage(byte[] message) {
    	SignupUserDetailsBean userDetails;
		try {
			userDetails = new ObjectMapper().readValue(new String(message), SignupUserDetailsBean.class);
			taskExecutor.execute(consumeMessage(userDetails));
		} catch (JsonProcessingException e) {
			LOG.error(e.getMessage());
		}
    }
    
    /**
     * This method have a logic on received message 
     * 
     * @param SignupUserDetailsBean userDetails
     */
    private Runnable consumeMessage(SignupUserDetailsBean userDetails) {
		return () -> {
				String phoneNumber = SignupUtil.formattedPhoneNumber(userDetails.getPhone());
				String userName = SignupUtil.formattedName(userDetails.getFullName());

				LOG.info(String.format(SignupConstants.AKNOWLEDGE, userName, phoneNumber));
		};
	}
}
