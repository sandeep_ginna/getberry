package com.message.users.signup.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.message.users.signup.exception.SignupUserCustomException;

@ControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * It will catch the SignupUserCustomException,
	 * when the user details are empty
	 * 
	 * @return ResponseEntity<?>
	 */
	@ExceptionHandler(value = SignupUserCustomException.class)
	public ResponseEntity<?> handleCustamUserException(){
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
}
