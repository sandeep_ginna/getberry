package com.message.users.signup.service;

import com.message.users.signup.model.SignupUserDetailsBean;

public interface ISignupMessagePublisher {

	void publish(SignupUserDetailsBean userDetailsBean);
}
