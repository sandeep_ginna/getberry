package com.message.users.signup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.message.users.signup.model.SignupUserDetailsBean;
import com.message.users.signup.service.ISignupMessagePublisher;

@RestController
public class SignupMessageController {

    @Autowired
    private ISignupMessagePublisher messagePublisher;

    /**
	 * This method will invoked when user sends the request using localhost:8080.
	 * If the request is in right format then it will give HTTP status 200 OK
	 * If the request is having validation errors than will return with HTTP status 400 BAD_REQUEST
     * @param userDetailsBean
     * @return ResponseEntity<?>
     */
    @PostMapping(value="/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> userDetails(@RequestBody SignupUserDetailsBean userDetailsBean) {
    		messagePublisher.publish(userDetailsBean);
    		return ResponseEntity.status(HttpStatus.OK).build();
    }

}
