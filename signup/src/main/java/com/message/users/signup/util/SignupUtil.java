package com.message.users.signup.util;

import org.springframework.util.StringUtils;

import com.message.users.signup.exception.SignupUserCustomException;
import com.message.users.signup.model.SignupUserDetailsBean;

public class SignupUtil {

	
	/**
	 * This method will validate the User name and Phone number.
	 * is empty or null
	 * @param SignupUserDetailsBean
	 * @exception SignupUserCustomException
	 */
	public static void validateUser(SignupUserDetailsBean userDetailsBean) {
		if ( StringUtils.isEmpty(userDetailsBean.getFullName())) {
			throw new SignupUserCustomException(String.format(SignupConstants.SIGNUP_USER_INVALID, "Entered User Name"));
		}

		if (StringUtils.isEmpty(userDetailsBean.getPhone())) {
			throw new SignupUserCustomException(String.format(SignupConstants.SIGNUP_USER_INVALID, "Entered Phone Number"));
		}
	}
	
	
	/**
	 * This method will split the entered user name based on the spaces
	 * and return if FirstName and LastName.
	 * @param userName
	 * @return
	 */
	public static String formattedName(String userName) {

		String space = " ";
		String[] userNameArray = userName.split(space);

		if (userNameArray.length == 1) {
			return userName;
		} else {
			return userNameArray[0] + space + userNameArray[userNameArray.length - 1];
		}
	}
	
	/**
	 * The method will remove the spaces and
	 * append +33 if the given phone number doesn't starts with.
	 * @param phoneNumber
	 * @return phoneNumber String
	 */
	public static String formattedPhoneNumber(String phoneNumber) {		
		phoneNumber = StringUtils.trimAllWhitespace(phoneNumber);
		if (phoneNumber.startsWith(SignupConstants.PREFIX)) {
			return phoneNumber;
		} else {
			return SignupConstants.PREFIX + phoneNumber;
		}
	}

}

