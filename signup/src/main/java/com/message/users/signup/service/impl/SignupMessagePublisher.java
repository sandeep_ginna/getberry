package com.message.users.signup.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.message.users.signup.model.SignupUserDetailsBean;
import com.message.users.signup.util.SignupUtil;

@Service
public class SignupMessagePublisher {

	private static final Logger LOG = LogManager.getLogger(SignupMessagePublisher.class);

	@Autowired
	private RabbitTemplate rabbitTemplate;

	/**
	 * It will publish the message in RabbitMQ server 
	 * 
	 * @param SignupUserDetailsBean userDetailsBean
	 */
	public void publish(SignupUserDetailsBean userDetailsBean) {
		SignupUtil.validateUser(userDetailsBean);
		rabbitTemplate.convertAndSend(userDetailsBean);
		LOG.debug("Publlished Message");
	}
}
