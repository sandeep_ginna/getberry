package com.message.users.signup.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class SignupUserCustomException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SignupUserCustomException(String message) {
		super(message);
	}
}
