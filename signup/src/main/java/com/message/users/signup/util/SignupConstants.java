package com.message.users.signup.util;

public class SignupConstants {

	public static final String PARSING_ERROR = "Exception occured while Signup User";
	public static final String SIGNUP_USER_INVALID = "%s is Invalid";
	public static final String AKNOWLEDGE = "User %s with phone %s has just SignedUp!";
	public static final String PREFIX = "+33";
	
}
