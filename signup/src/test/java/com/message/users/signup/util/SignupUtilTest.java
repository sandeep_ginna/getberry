package com.message.users.signup.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.message.users.signup.exception.SignupUserCustomException;
import com.message.users.signup.model.SignupUserDetailsBean;

public class SignupUtilTest {

	@Test
	void testFormatName() throws SignupUserCustomException {
		assertEquals("John Doe", SignupUtil.formattedName("John Doe"));
	}

	@Test
	void testFormatNameWithMiddleName() throws SignupUserCustomException {
		assertEquals("John Klain", SignupUtil.formattedName("John Doe Klain"));
	}

	@Test
	void testFormatNameWithEmptyName() {

		Assertions.assertThrows(SignupUserCustomException.class, () -> {
			SignupUtil.validateUser(SignupUserDetailsBean.newSignupUserDetailsBean().firstName("").build());
		});
	}

	@Test
	void getPhoneNumberInFrenchNumberFormatWhereNumberAlreadyFormatted() {
		Assertions.assertThrows(SignupUserCustomException.class, () -> {
			SignupUtil.validateUser(SignupUserDetailsBean.newSignupUserDetailsBean().phone("").build());
		});
	}

	@Test
	@ExceptionHandler(value = SignupUserCustomException.class)
	void testAddPrefixToPhoneNumber() {

		assertEquals("+33546317171", SignupUtil.formattedPhoneNumber("546317171"));
	}

	@Test
	@ExceptionHandler(value = SignupUserCustomException.class)
	void testAddPrefixToPhoneNumberWithSpaces() {

		assertEquals("+33546317171", SignupUtil.formattedPhoneNumber("5 46 31 71 71"));
	}

}